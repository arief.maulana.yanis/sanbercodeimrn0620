//Soal No. 2 (Promise Baca Buku)
//===============================
console.log("//Soal No. 2 (Promise Baca Buku))");
console.log("//===============================");
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var time=10000
var i=0;
function letsread(time,books){
    if(time>0 && i<books.length)
    readBooksPromise(time,books[i])
        .then(function (sisaWaktu) {
            time=sisaWaktu;
            i++;
            letsread(time,books);
        })
        .catch(function (sisaWaktu) {
            return;
        });
}

letsread(time,books)