// Code di sini
function rangeWithStep(startNum, finishNum, step) {
    var output=[];
    if(startNum<finishNum){
        while(startNum<=finishNum){
            output.push(startNum);
            startNum=startNum+step;
        }
    }
    else{
        while(startNum>=finishNum){
            output.push(startNum);
            startNum=startNum-step;
        }
    }
    return output;
}

function sum(awal,akhir,step){
    var output=0;
    if(typeof akhir==="undefined")
        akhir=awal;
    if(typeof step==="undefined")
        step=1;
    var range=rangeWithStep(awal,akhir,step);

    range.forEach(function(number) {
        output=output+number;
    }, range);
    /*
    if(awal<=akhir){
        while(awal<=akhir){
            output=output+awal;
            awal=awal+step;
        }
    }
    else{
        while(awal>=akhir){
            output=output+awal;
            awal=awal-step;
        }
    }
    */
    return output;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 