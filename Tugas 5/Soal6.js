function NamaBulan(bulan){
    var namabulan;
    switch(bulan){
        case 1: {namabulan="Januari";break;}
        case 2: {namabulan="Februari";break;}
        case 3: {namabulan="Maret";break;}
        case 4: {namabulan="April";break;}
        case 5: {namabulan="Mei";break;}
        case 6: {namabulan="Juni";break;}
        case 7: {namabulan="Juli";break;}
        case 8: {namabulan="Agustus";break;}
        case 9: {namabulan="September";break;}
        case 10: {namabulan="Oktober";break;}
        case 11: {namabulan="Nopember";break;}
        case 12: {namabulan="Desember";break;}
        default: {namabulan="";}
    }
    return namabulan;
}


function dataHandling2(input){
    //name
    var arr=input[1].trim().split(" ");
    var count=arr.push("Elsharawy");
    input[1]=arr.join(" ");
    //provinsi
    var arr=input[2].trim().split(" ");
    var count=arr.unshift("Provinsi");
    input[2]=arr.join(" ");
    //add gender,school
    input.splice(4,1,"Pria", "SMA Internasional Metro");
    console.log(input);
    
    //birth month
    var date=input[3];
    var arrDate=date.split("/");
    console.log(NamaBulan(Number(arrDate[1])));

    //sort
    var arrSort=arrDate;
    arrSort.sort(function(a, b){return b - a});
    console.log(arrSort);

    //join
    var name=input[1];
    console.log(name.slice(0,15));
}

//contoh output
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);