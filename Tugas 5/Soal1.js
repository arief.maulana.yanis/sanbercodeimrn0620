// Code di sini
function range(startNum, finishNum) {
    if(typeof startNum === "undefined" || typeof finishNum ==="undefined")
        return -1
    else{
        var output=[];
        if(startNum<=finishNum){
            for(var i=startNum;i<=finishNum;i++){
                output.push(i);
            }
        }
        else{
            for(var i=startNum;i>=finishNum;i--){
                output.push(i);
            }
        }
        return output;
    }
}


console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
