var tanggal=21; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan=1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun=1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if(tanggal<1 || tanggal>31)
    console.log("Tanggal tidak valid");
else{
    if(bulan<1 || bulan>12)
        console.log("Bulan tidak valid");
    else{
        if(tahun<1900 || tahun>2200)
            console.log("Tahun tidak valid");
        else{
            var namabulan;
            switch(bulan){
                case 1: {namabulan="Januari";break;}
                case 2: {namabulan="Februari";break;}
                case 3: {namabulan="Maret";break;}
                case 4: {namabulan="April";break;}
                case 5: {namabulan="Mei";break;}
                case 6: {namabulan="Juni";break;}
                case 7: {namabulan="Juli";break;}
                case 8: {namabulan="Agustus";break;}
                case 9: {namabulan="September";break;}
                case 10: {namabulan="Oktober";break;}
                case 11: {namabulan="Nopember";break;}
                case 12: {namabulan="Desember";break;}
                default: {namabulan="";}
            }
            console.log(tanggal+" "+namabulan+" "+tahun);
        }
    }
}