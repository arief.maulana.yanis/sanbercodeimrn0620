//1. Mengubah fungsi menjadi fungsi arrow
//=======================================
console.log("1. Mengubah fungsi menjadi fungsi arrow")
console.log("=======================================")
//JS
/*
const golden = function goldenFunction(){
  console.log("this is golden!!")
}
*/
//ES6
const golden = () => {
    console.log("this is golden!!") 
}

golden()

//2. Sederhanakan menjadi Object literal di ES6
//=============================================
console.log("")
console.log("2. Sederhanakan menjadi Object literal di ES6")
console.log("==============================================")
// JS
/*
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
*/
//ES6
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName)
      return 
    }
  }    
}
//Driver Code 
newFunction("William", "Imoh").fullName() 

//3. Destructuring
//================
console.log("")
console.log("3. Destructuring")
console.log("================")

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
// JS
/*
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
*/
//ES6
const {firstName, lastName, destination, occupation} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)

//4. Array Spreading
//==================
console.log("")
console.log("4. Array Spreading")
console.log("==================")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// JS
/*
const combined = west.concat(east)
*/
//ES6
const combined = [...west, ...east]
//Driver Code
console.log(combined)

//5. Template Literals
//====================
console.log("")
console.log("5. Template Literals")
console.log("====================")

const planet = "earth"
const view = "glass"
// JS
/*
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
*/
//ES6
var before = `Lorem ${view}dolor sit amet, `+
 `consectetur adipiscing elit,${planet}do eiusmod tempor `+
 `incididunt ut labore et dolore magna aliqua. Ut enim`+
 ` ad minim veniam`
// Driver Code
console.log(before) 